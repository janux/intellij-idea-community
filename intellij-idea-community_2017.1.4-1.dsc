-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2017.1.4-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 2af60d7bb0d2765946394c4200dac6e38abe4b29 5641 intellij-idea-community_2017.1.4.orig.tar.gz
 d41a6b865635433be57c41defcfdf007007e5a5c 2632 intellij-idea-community_2017.1.4-1.debian.tar.xz
Checksums-Sha256:
 93646f0ed33fb1c62473e12c18f9fa7d90ade1ec3ec2c00b92eafd9fac728882 5641 intellij-idea-community_2017.1.4.orig.tar.gz
 1d722750f344c5c78b16448f094bece620dacc23d35ae02cb56668e777a4a761 2632 intellij-idea-community_2017.1.4-1.debian.tar.xz
Files:
 38a2df9af92629b31beaa46222d3e00e 5641 intellij-idea-community_2017.1.4.orig.tar.gz
 f7e4eb40ecc1b5c8e5f5daa792170f17 2632 intellij-idea-community_2017.1.4-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIcBAEBCAAGBQJZOQZvAAoJECuF2UqwCcGY+8AQALcFghKPeFRbSq+lR7oZz3gP
itL0szWQXwAAyec3ji62eczJumWYEk9DIs1loYSEXFD/IdMuo7EB+sDXwOo5QR5A
bgwleBm5o60ahRTf09YnhY3b6Wa2g0KSMQfzKdi5eCYBatQNGbo7UtD1MGJB2Y8+
cthe5CvH8zp1X6BQ+VFxlS0mbJ3ppzmRGiG9+hIpyQOh7avmJpFaECc13V5I5v6j
DRzOfxCrpmK93HC9gR+oXftkYnlApQlu4wWtCy6r7TSv31/iUsuen7hYN4k4o0Si
2xVzNyZvEUdU+Y8wsw3xnAF6tDgzHc/j5G1qZCad7yvyHIP5Vn7I+oFDPR8Qr6HA
frITikDtTwi0EHQQvF9znWsheWoUSypxTS0m1agO+PV1dp+G9uN2phenYGasIAR0
6QILMchJ36MEtMF4ij6YCEJkIVD3mWXVaWas2n5DBx4eW5KcszxUiknzERD6LrPU
pwu9BpS4GuFnhgHqbMrMeC77wDq3kCPZI8NJ5qrGzwk54r9dRtmrSyJKLp6jlJ9r
W/s0xAts4+rNEzdTkfkgyniA1bax+1jYgzAi2APq3b3qUjOanPbxr9sLC5qH/Thu
2YB1Xh0nacbW7g59BN7Ca/JVtz5xmjNoAMl2mYJTzfdHYhiK/2N+TPbEObdroeJl
2KlHXk1NK2AmSRwk9+UU
=Pp6b
-----END PGP SIGNATURE-----
